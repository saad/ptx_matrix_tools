# ptx_matrix_tools

A set of tools (for now just the one) for processing matrix1,2,3 after Probtrackx.


## Getting started
Create a conda environment (this is not strictly necessary but can help keep your environments clean):

```commandline
conda create -n ptxmat python==3.10
conda activate ptxmat
```

Clone repo and install:
```commandline
git clone https://git.fmrib.ox.ac.uk/saad/ptx_matrix_tools.git
cd ptx_matrix_tools
pip install .
```

## Running the script on a single subject
To get help with the script, type e.g.:

```commandline
ptx_matrix_decomp --help
```

An example use of this script is if you have run FSL's `probtrackx2` with the `--omatrix2` option and you want to analyse the output. The `ptx_matrix_decomp` script runs ICA on the matrix2 output (with careful treatment of high dimensional matrices). The results are a set of components which are typically grey matter maps (seeds) and white matter maps (targets). See theory below.

An example run:

```commandline
ptx_matrix_decomp --ptxdir <my_probtrackx_folder> --dim 10 --migp 400
```

The above command will load the `fdt_matrix2.dot` file, reduce its column dimension using MIGP (keeping 400 dims), and run ICA on the result, keeping 10 dimensions.
 
A typical structure of a `probtrackx2` directory when using `--omatrix2`:

```
ptx_folder
    |
    +-- coords_for_fdt_matrix2
    +-- fdt_matrix2.dot
    +-- fdt_paths.nii.gz
    +-- lookup_tractspace_fdt_matrix2.nii.gz
    +-- probtrackx.log
    +-- tract_space_coords_for_fdt_matrix2
    +-- waytotal
```

The script will decompose the `fdt_matrix2.dot` output, and save the results into the `G` and `W` matrices (see theory below). The `G` matrix correponds to the tract space, so can be NIFTI, GIFTI, or a combination. The `W` matrix is always a NIFTI volume. 

For example, if one uses `probtrackx2` with a seed that is a combination of surface and volume, e.g.:

```commandline
echo my_volume.nii.gz     > seed.txt
echo my_surface.surf.gii >> seed.txt
probtrackx2 --seed=seed.txt <other options>
```

Then the script will output (where `<dim>` is the chosen dimensionality):

```commandline
out_folder
    |
    +-- G_<dim>_0.nii.gz
    +-- G_<dim>_1.func.gii
    +-- W_<dim>.nii.gz
```

## Running for multiple subjects

The `ptx_matrix_decomp` script can also run on multiple subjects' ptx folders:

```commandline
ptx_matrix_decomp --ptxdir <ptx_folder_subj1> <ptx_folder_subj2> ... --ptx_seeds <my_seed_mask_1> <my_seed_mask_2> ... 
```

Note that for a single subject call, the script can look for the location of the seed files inside the probtrackx logfile, but for multi-subject runs, the seed masks must be provided explicitly in the command line.

The script will run ICA on the group average matrix, then optionally run dual regression to obtain individual subjects' versions of `G` and `W` (see theory below)

An example output for two subjects including dual regression and winner-takes-all on the group average (notice there are two `G` files corresponding to a call to `probtrackx` with two seed file, one volumetric (NIFTI) and one surface (GIFTI)):

```commandline
out_folder    
    +-- G_dim20_0.nii.gz
    +-- G_dim20_1.func.gii        
    +-- W_dim20.nii.gz    
    +-- dualreg_on_G
        +-- Gs_000_dim20_0.nii.gz
        +-- Gs_000_dim20_1.func.gii
        +-- Gs_001_dim20_0.nii.gz
        +-- Gs_001_dim20_1.func.gii
        +-- Ws_000_dim20.nii.gz
        +-- Ws_001_dim20.nii.gz
    +-- dualreg_on_W    
        +-- Gs_000_dim20_0.nii.gz
        +-- Gs_000_dim20_1.func.gii
        +-- Gs_001_dim20_0.nii.gz
        +-- Gs_001_dim20_1.func.gii
        +-- Ws_000_dim20.nii.gz
        +-- Ws_001_dim20.nii.gz
```

## Running a GLM

When in dual regression mode, you can provide a design matrix and contrast matrix to run a GLM. These can be create with FSL's GLM GUI, or can be simple ASCII text files saved from Python or Matlab (or Excel!).

Here is an example command to run a GLM and the output folder structure:

```commandline
ptx_matrix_decomp --ptxdir <subj1> <subj2> <subj3> --dim 10 --mode dualreg --design_mat <design.mat> --design_con <design.con>
```

And the output folder includes two GLM folders, one for dual reg on `G` and one for dual reg on `W`:

```
out_folder
    +-- G_dim10_0.nii.gz
    +-- W_dim10.nii.gz
    +-- glm_on_G        
        +--- G_xstat1.nii.gz
        +--- G_xstat2.nii.gz
        ... etc.
    +-- glm_on_W
        +-- etc.
```


## Theory
# From matrix2 to outer product decomposition model

We start with a collection of connectivity matrices $\{C_s\}$ (one per subject) of size $g\times w$ where $g$ is the number of grey matter voxels (or vertices), and $w$ the number of voxels in the white matter.

We then form the average matrix $C=\lt C_s \gt$. 

We aim to decompose $C$ using an outer product model:

$C = GW^T$

Where the columns of $G$ are grey matter maps and the columns of $W$ are white matter tracts. Then we aim to find the subject-specific versions of such matrices. 

Let's start with the average matrix $C$. The problem is that both $g$ and $w$ are potentially large numbers, which means the outer product decomposition will struggle. 

The approach we follow below is the following:

- Reduce the dimensionality of $C$ using [incremental PCA](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4289914/) (where incrementation is done on the columns) to obtain $\tilde{C}$ of dimension $g\times k$ where $k$ is smaller than $w$.
- Decompose $\tilde{C}$ with an outer product model, to obtain $\tilde{C}=G\tilde{W}^T$. The matrices $G$ and $\tilde{W}$ have sizes $g\times p$ and $k\times p$ respectively, where $p$ is the number of 'components' (chosen by the user).
- $G$ now contains the desired grey matter maps. We are done with $G$.
- To obtain the white matter maps $W$, we need an additional step, which is to regress $G$ against $C$ (the original connectivity matrix): $W=G^{\dagger}C$.

Now we turn to the individuals matrices $C_s$ and their decompositions into $G_sW_s^T$.

To make sure that these decompositions are compatible across subjects, we can use dual regression in two different ways:

- 1. Where $G$ (grey matter maps) from the group average is used to drive the regression: 

$$
\begin{array}{lcr}
W_s^T & = & G^{\dagger}C_s  \\
G_s^T & = & W_s^{\dagger}C_s^T
\end{array}
$$


- 2. Where $W$ (white matter maps) from the group average is used to drive the regression:

$$
\begin{array}{lcr}
G_s^T & = & W^{\dagger}C_s^T  \\
W_s^T & = & G_s^{\dagger}C_s
\end{array}
$$




