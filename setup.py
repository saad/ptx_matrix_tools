#!/usr/bin/env python

from setuptools import setup

with open('requirements.txt', 'rt') as f:
    install_requires = [line.strip() for line in f.readlines()]

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='ptx_matrix_tools',
      description='Tool for processing Probtrackx Matrix output',
      author='Saad Jbabdi & Rogier Mars',
      author_email='<saad.jbabdi@ndcn.ox.ac.uk> and <rogier.mars@ndcn.ox.ac.uk>',
      url='www.fmrib.ox.ac.uk/fsl',
      packages=['ptx_matrix_tools',],
      install_requires=install_requires,
      scripts=['ptx_matrix_tools/scripts/ptx_matrix_decomp',]
      )

