#!/usr/bin/env python

# utils.py - helper functions for ptx_matrix_tools
#
# Author: Saad Jbabdi <saad@fmrib.ox.ac.uk>
#         Aref Miri Rekavandi
#
# Copyright (C) 2023 University of Oxford
# SHBASECOPYRIGHT


# Miminize the number of imports
import numpy as np
import torch
import os
from tqdm import tqdm
from scipy.special import xlogy

# the line below is to prevent the notebook from crashing -
# ... not sure if it is needed outside the notebook
os.environ['KMP_DUPLICATE_LIB_OK']='True'

class MultiViewLoss(object):
    """This class takes care of calculating distances
    """
    def __init__(self, high_dim_loss = 'euclidean', low_dim_loss = 'euclidean'):
        self.same_loss = True if high_dim_loss == low_dim_loss else False
        self.HDL = self.loss(high_dim_loss)
        self.LDL = self.loss(low_dim_loss)

    def loss(self, loss_type):
        if loss_type.lower() == 'euclidean':
            return self.euclidean
        elif loss_type.lower() == 'kl':
            return self.kl
        else:
            raise(Exception(f'Unknown loss {loss_type}'))

    def H_v_L(self, D, d, weighted=False, max_dist=1e2):
        if not self.same_loss:
            D = self.normalise(D)
            d = self.normalise(d)
        if not weighted:
            return torch.mean( (D-d)**2 )
        else:
            w = max_dist*np.ones_like(D)-D
            return torch.mean( (D-d)**2 )

    @staticmethod
    def normalise(X):
        return (X-torch.mean(X))/torch.std(X)

    @staticmethod
    def euclidean(X, Y):
        return torch.mean( (X-Y)**2, axis=1 )

    @staticmethod
    def kl(X, Y):
        assert X.min()>=0
        assert Y.min()>=0
        X = X / torch.sum(X, axis=1, keepdim=True)
        Y = Y / torch.sum(Y, axis=1, keepdim=True)

        X_nz = X.clone()
        Y_nz = Y.clone()

        X_nz[X==0]=1 # to prevent log(0)
        Y_nz[Y==0]=1 # to prevent log(0)

        symkl =  torch.sum( xlogy(X, X) - xlogy(X,Y_nz), axis=1)
        symkl += torch.sum( xlogy(Y, Y) - xlogy(Y,X_nz), axis=1)
        symkl /= 2.
        return symkl


# define the encoder network below
class MultiViewEmbedding(torch.nn.Module):
    def __init__(self,
                 input_size,
                 output_size=2,
                 hidden_size=10,
                 num_hidden=1,
                 nonlin=torch.nn.Tanh,
                 input_dist='euclidean',
                 output_dist='euclidean',
                 include_within=False
                ):
        """Encoder class
        Input -> Linear+nonlin -> Linear+nonlin -> .... -> Linear-> Ouput


        Params:
        input_size (int)  : dimension of feature space
        output_size (int) : dimension of embedding space
        hidden_size (int) : width of each layer (must be>0)
        num_hidden (int)  : number of hidden layer (must be>0)
        nonlin (callable) : Torch nonlin function
        input_dist (str)  : distance in input space
        output_dist (str) : distance in output space
        include_within (bool) : include within-view in loss function
        """
        super().__init__()
        assert num_hidden > 0
        assert output_size > 0

        # Network-related
        lin_tanh_layers = [torch.nn.Linear(input_size, hidden_size),nonlin()]
        for i in range(num_hidden):
            if i < num_hidden-1:
                lin_tanh_layers.extend( [torch.nn.Linear(hidden_size, hidden_size),nonlin()]  )
            else:
                lin_tanh_layers.append( torch.nn.Linear(hidden_size, output_size) )
        self.lin_tanh_layers = torch.nn.Sequential(*lin_tanh_layers)

        # Loss-related
        self.MVloss = MultiViewLoss(input_dist, output_dist)
        self.include_within = include_within

    def forward(self, batch):
        """Apply forward model
        input : tensor (Nxinput_dim)
        output : tensor (Nxoutput_dim)

        Params:
        batch (dict) : each batch[key] is a Nxp array
        """
        return self.lin_tanh_layers(batch)

    def batchtrain(self, data, num_epochs=64, batch_size=512, learning_rate = 0.001):
        """Training the network with SGD
        Params:
        data : dict ( dict of data matrices )
        Returns:
            record (sum of losses per epoch)
            all losses as list
        """

        optimizer = torch.optim.SGD(self.parameters(), lr=learning_rate)
        # dataloader
        dataloader = RandomDataLoader(data, batch_size=batch_size)
        # begin training
        record = []
        all_losses = []

        self.train()
        for epoch in tqdm(range(num_epochs)):
            loss_record = 0.
            for batch in dataloader:
                # zero de gradients
                optimizer.zero_grad()
                # encode batch
                encoded = [self.forward(b) for b in batch]
                # get loss
                loss_value = self.loss(batch, encoded)
                # record loss
                all_losses.append(loss_value.item() )
                loss_record += loss_value.item()
                # learn
                loss_value.backward()
                optimizer.step()
            record.append( loss_record )

        return record, all_losses

    def loss(self, batch, encoded, weighted=False):
        """Loss function
        batch and encoded matrices are compared row-wise.
        Params:
            batch (list) : list of highdim matrices
            encoded (list) : list of lowdim matrices
        Returns:
            float
        """
        D = torch.tensor([0.], dtype=torch.float32)
        for i, (M1,m1) in enumerate(zip(batch,encoded)):
            start = i if self.include_within else i+1
            for j, (M2,m2) in enumerate(zip(batch[start:],encoded[start:])):
                if self.include_within and start==i:
                    M2, m2 = self.shuffle_rows(M2,m2)
                dist_highdim = self.MVloss.HDL(M1,M2)
                dist_lowdim = self.MVloss.LDL(m1,m2)
                D += self.MVloss.H_v_L(dist_highdim, dist_lowdim, weighted)

        return D

    @staticmethod
    def shuffle_rows(M, m):
        """Shuffle the rows of matrices M and m using the same randomization
        """
        idx = np.random.permutation(M.shape[0])
        return M[idx,:], m[idx,:]

class RandomDataLoader():
    def __init__(self, data_dict, batch_size):
        """My DataLoader iterable
        can iterate through to give random lists of input data matrices
        """
        self.data_dict = self.preprocess(data_dict)
        self.batch_size = batch_size
        self.sizes = [data_dict[p].shape[0] for p in data_dict]
        self._length = max(self.sizes)
        self._index = 0
        self.rng = np.random.default_rng()
    def __iter__(self):
        return self
    def __next__(self):
        if self._index < self._length:
            indices = [ self.rng.integers(0, self.data_dict[key].shape[0], size=self.batch_size) for key in self.data_dict ]
            batch = [torch.tensor(self.data_dict[key][idx,:]) for idx,key in zip(indices, self.data_dict)]
            self._index += self.batch_size
            return batch
        else:
            # reset
            self._index = 0
            self._length = self.sizes[0]
            raise StopIteration
    @staticmethod
    def preprocess(data_dict):
        """Do some preprocessing
        For now, just remove all rows that are identically zero and convert to float32

        """
        clean_dict = data_dict.copy()
        for key in data_dict:
            tmp = np.array(data_dict[key], dtype=np.float32)
            row_sum = np.sum(np.abs(tmp), axis=1)
            clean_dict[key] = tmp[row_sum>0,:]
        return clean_dict




