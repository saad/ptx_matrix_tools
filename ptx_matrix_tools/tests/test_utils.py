#!/usr/bin/env python

# TODO : increase coverage

import numpy as np
import os
import time
from ptx_matrix_tools import utils


from pathlib import Path
testsPath = Path(__file__).parent


def test_timer():
    t = utils.timer()
    t.tic()
    time.sleep(3)
    assert np.isclose(float(t.toc()), 3, rtol=.1)

def test_load_mat2():
    fname = testsPath / 'test_data' / 'example_mat2.dot'
    M = utils.load_mat2(fname)
    assert M.shape[0] == 1000
    assert M.shape[1] == 500
    assert np.round(np.mean(M)) == 40
    # check float
    fname = testsPath / 'test_data' / 'example_mat2_small.dot'
    M = utils.load_mat2(fname)
    assert M.shape == (2,2)
    assert np.all(np.isclose(M, np.array([[0., 2.2],[0., 2.]]), rtol=1e-3))


def test_avg_matrix2():
    fname = testsPath / 'test_data' / 'example_mat2.dot'
    M = utils.avg_matrix2([fname, fname, fname])
    assert M.shape[0] == 1000
    assert M.shape[1] == 500
    assert np.round(np.mean(M)) == 40

def test_demean():
    X = np.random.randn(100,50)
    assert np.isclose( np.linalg.norm( np.mean(utils.demean(X, axis=0), axis=0)), 0, rtol=1e-3 )
    assert np.isclose( np.linalg.norm( np.mean(utils.demean(X, axis=1), axis=1)), 0, rtol=1e-3 )

def test_matrix_MIGP():
    import warnings
    warnings.filterwarnings('ignore')
    C = np.random.randn(100,1000)
    Csmall = utils.matrix_MIGP(C, n_dim=20, keep_mean=True)
    assert Csmall.shape[1] == 20
    Csmapp = utils.matrix_MIGP(C, n_dim=20, keep_mean=False)
    assert Csmall.shape[1] == 20

def test_SignFlip():
    X = np.array([[-2, -2, -2, 1, 1, 1],
                  [2, 2, 2, -1, -1, -1],
                  [1, 1, 1, 1, 1, 1],
                  [-2, -2, -2, -2, -2, -2]])
    XX = utils.SignFlip(X)
    assert XX.shape == X.shape
    assert np.all(np.isclose(XX[0], np.array([2, 2, 2, -1, -1, -1])))
    assert np.all(np.isclose(XX[1], np.array([2, 2, 2, -1, -1, -1])))
    assert np.all(np.isclose(XX[2], np.array([1, 1, 1, 1, 1, 1])))
    assert np.all(np.isclose(XX[3], np.array([2, 2, 2, 2, 2, 2])))

def test_matrix_decomposition():
    C = np.random.randn(100, 1000)**3
    import warnings
    warnings.filterwarnings('ignore')
    utils.matrix_decomposition(C, n_components=10, normalise=True, algo='ICA')
    utils.matrix_decomposition(C, n_components=10, normalise=False, algo='ICA')
    utils.matrix_decomposition(np.abs(C), n_components=10, do_migp=False, normalise=False, algo='NMF')

def test_dualreg():
    Cs = np.random.randn(100,1000)**3
    X  = np.random.randn(100,20)**3
    a, b = utils.dualreg(Cs, X, normalise=False)
    assert a.shape == (100, 20)
    assert b.shape == (20, 1000)
    X  = np.random.randn(20,1000)**3
    a, b = utils.dualreg(Cs, X, normalise=True)
    assert a.shape == (100, 20)
    assert b.shape == (20, 1000)

def test_winner_takes_all():
    X = np.random.randn(100,10)
    x = utils.winner_takes_all(X, axis=0, z_thr=2)
    assert x.shape == (1, 10)
    x = utils.winner_takes_all(X, axis=1, z_thr=1)
    assert x.shape == (100, 1)

def test_mat2vol():
    matfile = testsPath / 'test_data' / 'fdt_matrix2.dot'
    lut_file = testsPath / 'test_data' / 'lookup_tractspace_fdt_matrix2'
    mat = utils.load_mat2(matfile)
    from fsl.data.image import Image
    lut_vol = Image(lut_file)
    utils.mat2vol(mat, lut_vol)

def test_is_gifti():
    assert utils.is_gifti(testsPath / 'test_data' / 'example_gifti.gii')
    assert not utils.is_gifti(testsPath / 'test_data' / 'lookup_tractspace_fdt_matrix2')

def test_is_nifti():
    assert not utils.is_nifti(testsPath / 'test_data' / 'example_gifti.gii')
    assert utils.is_nifti(testsPath / 'test_data' / 'lookup_tractspace_fdt_matrix2')

def test_read_ascii_list():
    l = utils.read_ascii_list(testsPath / 'test_data' / 'ascii_list.txt')
    assert len(l) == 3

def test_is_ptx_log():
    assert utils.is_ptx_log( testsPath / 'test_data' / 'probtrackx.log' )
    assert not utils.is_ptx_log( testsPath / 'test_data' / 'ascii_list.txt' )

def test_get_seed():
    assert len(utils.get_seed( testsPath / 'test_data' / 'ptxdir_seed', check_exists=False )) == 1
    assert len(utils.get_seed( testsPath / 'test_data' / 'ptxdir_x', check_exists=False )) == 1

# GLM
def test_GLM():
    y = np.random.randn(10,1000)
    x = np.random.randn(10,3)
    glm = utils.GLM()
    glm.fit(x,y)
    c = [[1, 1, 1], [-1, 1, 0]]
    s = glm.calc_stats(c)
    assert s['tstat'].shape == (2, 1000)
    assert s['zstat'].shape == (2, 1000)
    assert s.pval.shape == (2, 1000)
    assert glm.fit_transform(x, y).shape == y.shape
    assert glm.transform(x).shape == y.shape
