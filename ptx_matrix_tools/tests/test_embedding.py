#!/usr/bin/env python

# TODO : increase coverage

import numpy as np
import os
import time
from ptx_matrix_tools import embedding
import torch

from pathlib import Path
testsPath = Path(__file__).parent

def test_MultiViewEmbedding():
    # Instantiate model
    model = embedding.MultiViewEmbedding(input_size=10,
                           hidden_size=10,
                           num_hidden=3 ,
                           output_size=2,
                           nonlin=torch.nn.Tanh,
                           input_dist='kl',
                           output_dist='euclidean',
                           include_within=True
                          )
    # Train model
    data = {'A' : np.random.rand(1000,10), 'B' : np.random.rand(1000,10), 'C' : np.random.rand(1000,10)}
    loss_epochs, losses = model.batchtrain(data, num_epochs=5, batch_size=100, learning_rate = 0.0002)
    assert np.all(~np.isnan(loss_epochs))
