#!/usr/bin/env python
# coding: utf-8

# In[5]:


import numpy as np
from matplotlib import pyplot as plt
from scipy.sparse.linalg import svds


# In[50]:


def connectivity_embedding(A, dims):
    # Row and column normalise
    ti = np.diag(np.sqrt(1/np.sum(A, axis=0)))
    to = np.diag(np.sqrt(1/np.sum(A, axis=1)))
    W = np.dot(np.dot(to, A), ti)
    # Singular value decomposition
    # NOT: U, S, V = np.linalg.svd(W)
    U, sdiag, VH = np.linalg.svd(W)
    S = np.zeros((A.shape[0], A.shape[1]))
    np.fill_diagonal(S, sdiag)
    V = VH.T
    # re-normalise and exclude first eigenvectors
    P = np.dot(to, U[:, 1: dims + 1])
    Q = np.dot(ti, V[:, 1: dims + 1])
    return P, Q

def normalise(M, axis=0):
    return (M - np.mean(M, axis=axis,keepdims=True)) / np.std(M, axis=axis,keepdims=True) / np.sqrt(M.shape[axis])

def svd_prod(A,B):
    k = A.shape[1]-1
    Ua, diag, VHa = svds(A, k=k, random_state=0)
    Ua = Ua[:,0:k]
    Sa = np.zeros((diag.shape[0], diag.shape[0]))
    np.fill_diagonal(Sa, diag)
    Ub, diag, VHb = svds(B, k=k, random_state=0)
    Ub = Ub[:,0:k]
    Sb = np.zeros((k, k))
    np.fill_diagonal(Sb, diag)
    Ui, diag, VHi = np.linalg.svd((Sa.dot(VHa).dot(VHb.T).dot(Sb.T)))
    Ui = Ui[:,0:k]
    Si = np.zeros((k, k))
    np.fill_diagonal(Si, diag)
    U = Ua.dot(Ui)
    V = Ub.dot(VHi.T)
    S = Si
    return [U,S,V]


def conem_eco(A,B, n_dims):
    p = A.shape[1]
    ti = 1/np.sqrt(B.dot(np.sum(A, axis=0)))
    to = 1/np.sqrt(A.dot(np.sum(B, axis=0)))
    WA = np.repeat(np.expand_dims(to, axis=1), p, axis=1) * A
    WB = np.repeat(np.expand_dims(ti, axis=1), p, axis=1) * B
    [U,a,V] = svd_prod(WA,WB)
    P = np.repeat(np.expand_dims(to, axis=1), n_dims, axis=1) * U[:,1:n_dims+1]
    Q = np.repeat(np.expand_dims(ti, axis=1), n_dims, axis=1) * V[:,1:n_dims+1]
    return[P,Q]


# In[81]:


n_features = 10
n_samples_A = 50000
n_samples_B = 400
n_dims = 2
np.random.seed(1337)

A = np.random.random((n_features, n_samples_A)).T
B = np.random.random((n_features, n_samples_B)).T


# In[82]:


# regular implementation, not 'eco'
# compute connectivity matrix
C = normalise(A, axis=0) @ normalise(B.T, axis=0)

# do embedding
p1,q1 = connectivity_embedding(C+1, n_dims)

p1[:,0] = -p1[:,0]
plt.imshow(p1.T)


# In[83]:


# with 'eco' implementation
p2,q2 = conem_eco(A,B,2)
plt.imshow(p2.T)


# In[80]:


plt.plot(p1[:,0], p2[:,0], 'x')
plt.plot(q1[:,1], q2[:,1], 'x')
plt.show(block=False)


# In[ ]:


# with large matrix
n_features = 6
n_samples_A = 50000
n_samples_B = 9027
n_dims = 2
np.random.seed(1337)

A = np.random.random((n_features, n_samples_A)).T
B = np.random.random((n_features, n_samples_B)).T
p,q = conem_eco(A,B,2)


# In[ ]:


# convert notebook to python script
# save notebook always before running this cell
get_ipython().system('jupyter nbconvert --to script svd_embedding.ipynb')

